//
//  PinchViewController.swift
//  AutoLayout
//
//  Created by Carlos Osorio on 27/6/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    @IBOutlet weak var heightC: NSLayoutConstraint!
    @IBOutlet weak var widthC: NSLayoutConstraint!
    
    var originalheight:CGFloat = 0
    var originalwidth:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        originalheight = heightC.constant
        originalwidth = widthC.constant

        // Do any additional setup after loading the view.
    }

    @IBAction func pinchGestureActivated(_ sender: Any) {
        let pinch = sender as! UIPinchGestureRecognizer
        
        //print("scale: \(pinch.scale)")
        //print("velocity: \(pinch.velocity)")
        
        heightC.constant = originalheight * pinch.scale
        widthC.constant = originalwidth * pinch.scale
        
        if pinch.state == .ended {
            
            UIView.animate(withDuration: 0.3){
            
            self.heightC.constant = self.originalheight
            self.widthC.constant = self.originalwidth
            }
        }
        
    }
    
}
