//
//  TapViewController.swift
//  AutoLayout
//
//  Created by Carlos Osorio on 27/6/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func tapGestureActivated(_ sender: Any) {
        mainView.backgroundColor = .black
    }
    
}
